#|/usr/bin/env python3

"""Sort a list of formats according to their level of compression"""
    
import sys

# Ordered list of image formats, from lower to higher compression
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    if fordered.index(format1) < fordered.index(format2):
        return True
    else:
        return False


def find_lower_pos(formats: list, pivot: int) -> int:
   lower = pivot
   for i in range(pivot + 1, len(formats)):
       if lower_than(formats[i], formats[lower]):
           lower = i
   return lower


def sort_formats(formats: list) -> list:
    for i in range(len(formats)):
        elemento = find_lower_pos(formats, i)
        formats[i], formats[elemento] = formats[elemento], formats[i]
    return formats


def main():
    """Read command line arguments, and print them sorted
    Also, check if they are valid formats using the fordered tuple"""

    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()


if __name__ == '__main__':
    main()
